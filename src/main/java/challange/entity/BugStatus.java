package challange.entity;

public enum BugStatus {
    NEW, VERIFIED, RESOLVED;
}
