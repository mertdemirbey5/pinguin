package challange.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class IssueInfoDTO {
    private String issueId;
    private String title;
    private String description;
    private LocalDateTime creationDate;
    private DeveloperDTO assignedTo;
}
