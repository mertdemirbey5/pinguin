package challange.dto;

import challange.entity.Story;
import lombok.Data;

@Data
public class WeekPlanItem {
    private Story story;
    private int hour;

    public WeekPlanItem(Story story, int hour) {
        this.story = story;
        this.hour = hour;
    }
}
