Running The Application:

System requirements: Java 8 JRE

The application binary is a "fat" jar file which means no external java library is needed to run the application.The fat jar of the projects are in the bin directory.(mbr-symbol-springboot-reactive-0.1.0.jar and mbr-symbol-springboot-0.1.0.jar)

$java -jar pinguin-challange-0.1.0.jar will run the the application on 8080 port. Application will be ready to accept rest requests. I have added a service which insert dummy object to test. That can be called from 

http://localhost:8080/api/insertdata   (get request)

http://localhost:8080/api/storyplan  (get request)

Other endpoints,

@GetMapping("/bugs")
@RequestMapping(value = "/bugs/{id}", method = RequestMethod.GET)
@DeleteMapping("/bugs/{id}")
@PostMapping("/bugs")
@RequestMapping(value = "/bugs", method = RequestMethod.PUT)
@PatchMapping("/bugs/{id}/developers/{developerId}")

@GetMapping("/developers")
@RequestMapping(value = "/developers/{id}", method = RequestMethod.GET)
@DeleteMapping("/developers/{id}")
@PostMapping("/developers")
@RequestMapping(value = "/developers", method = RequestMethod.PUT)

Unfortunetely(Story controller and dto is absent for now)

Running Building The Application:
System requirements:Java 8 SDK, gradle 4.0+ and optional IDE(Intellij or Eclipse)

$gradle build command to build the application this command will generate a fat jar under build/libs directory.

$gradle clean command to clean the project and generated binary jars.


Running The Application from the source
$gradle buildRun command will run the application from the source.
