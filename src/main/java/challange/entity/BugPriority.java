package challange.entity;

public enum BugPriority {
    CRITICAL, MAJOR, MINOR;
}
