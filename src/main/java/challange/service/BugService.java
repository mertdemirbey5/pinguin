package challange.service;

import challange.entity.Bug;

import java.util.List;
import java.util.Optional;

public interface BugService {
    List<Bug> findAll();

    Optional<Bug> findById(String id);

    void deleteById(String id);

    Bug save(Bug bug);

    Optional<Bug> updateBugOwner(String bugId, String developerId);
}
