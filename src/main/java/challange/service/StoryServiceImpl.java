package challange.service;

import challange.entity.Story;
import challange.entity.StoryStatus;
import challange.repository.StoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoryServiceImpl implements StoryService {
    @Autowired
    private StoryRepository storyRepository;

    @Override
    public List<Story> findAll() {
        return storyRepository.findAll();
    }

    @Override
    public Optional<Story> findById(String id) {
        return storyRepository.findById(id);
    }

    @Override
    public void deleteById(String id) {
        storyRepository.deleteById(id);
    }

    @Override
    public Story save(Story story) {
        return storyRepository.save(story);
    }

    @Override
    public List<Story> findEstimatedStories() {
        return storyRepository.findByStatus(StoryStatus.ESTIMATED);
    }

}
