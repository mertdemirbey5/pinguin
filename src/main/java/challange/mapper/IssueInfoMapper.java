package challange.mapper;

import challange.dto.IssueInfoDTO;
import challange.entity.IssueInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IssueInfoMapper implements Mapper<IssueInfoDTO, IssueInfo> {

    @Autowired
    private DeveloperMapper mapper;

    @Override
    public IssueInfo convertToEntity(IssueInfoDTO issueInfoDTO) {
        return IssueInfo.builder().issueId(issueInfoDTO.getIssueId())
                .assignedTo(mapper.convertToEntity(issueInfoDTO.getAssignedTo()))
                .creationDate(issueInfoDTO.getCreationDate())
                .description(issueInfoDTO.getDescription())
                .title(issueInfoDTO.getTitle())
                .build();
    }

    @Override
    public IssueInfoDTO convertToDTO(IssueInfo ent) {
        return IssueInfoDTO.builder().issueId(ent.getIssueId())
                .title(ent.getTitle())
                .description(ent.getDescription())
                .creationDate(ent.getCreationDate())
                .assignedTo(mapper.convertToDTO(ent.getAssignedTo()))
                .build();
    }
}
