package challange.mapper;

import challange.dto.BugDTO;
import challange.entity.Bug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BugMapper implements Mapper<BugDTO, Bug> {

    @Autowired
    private IssueInfoMapper issueInfoMapper;

    @Override
    public Bug convertToEntity(BugDTO bugDTO) {
        return Bug.builder().
                id(bugDTO.getId()).
                issueInfo(issueInfoMapper.convertToEntity(bugDTO.getIssueInfoDTO()))
                .status(bugDTO.getStatus())
                .priority(bugDTO.getPriority())
                .build();
    }

    @Override
    public BugDTO convertToDTO(Bug bug) {
        return BugDTO.builder().id(bug.getId())
                .issueInfoDTO(issueInfoMapper.convertToDTO(bug.getIssueInfo()))
                .status(bug.getStatus())
                .priority(bug.getPriority())
                .build();
    }
}
