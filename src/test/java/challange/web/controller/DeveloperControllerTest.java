package challange.web.controller;

import challange.dto.DeveloperDTO;
import challange.entity.Developer;
import challange.mapper.DeveloperMapper;
import challange.repository.DeveloperRepository;
import challange.service.DeveloperService;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DeveloperController.class)
public class DeveloperControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeveloperService service;

    @MockBean
    private DeveloperMapper mapper;

    @MockBean
    private DeveloperRepository repository;

    @Test
    public void givenGetDevelopersCalledThenReturnDevelopers() throws Exception {
        Developer john = Developer.builder().name("John").build();
        given(service.findAll()).willReturn(Arrays.asList(john));
        given(mapper.convertToDTO(john)).willReturn(new DeveloperDTO(john.getId(), john.getName()));
        mvc.perform(get("/api/developers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(john.getName())));
    }

    @Test
    public void givenDeveloperIdThatExistShouldReturnDeveloper() throws Exception {
        Developer john = Developer.builder().name("John").build();
        john.setId("41828a52-44b9-44f5-8bbd-7665c2d7fe52");
        given(service.findById(john.getId())).willReturn(Optional.of(john));
        given(mapper.convertToDTO(john)).willReturn(new DeveloperDTO(john.getId(), john.getName()));
        mvc.perform(get("/api/developers/" + john.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(john.getName())));
    }

    @Test
    public void givenSaveDeveloperCalledShouldReturnDeveloperDto() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO("97f40e95-880e-4cf8-ab79-cf3bb1dfe168", "John");
        Developer john = Developer.builder().name("John").build();
        john.setId(developerDTO.getId());
        given(service.save(john)).willReturn(john);
        given(mapper.convertToDTO(john)).willReturn(new DeveloperDTO(john.getId(), john.getName()));
        given(mapper.convertToEntity(developerDTO)).willReturn(john);
        mvc.perform(post("/api/developers/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(developerDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(developerDTO.getName())));

    }

    @Test
    public void givenDeleteMethodCalledShouldReturnSuccess() throws Exception {
        mvc.perform(delete("/api/developers/97f40e95-880e-4cf8-ab79-cf3bb1dfe168")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        Mockito.verify(service).deleteById("97f40e95-880e-4cf8-ab79-cf3bb1dfe168");
    }
}
