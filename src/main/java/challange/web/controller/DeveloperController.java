package challange.web.controller;

import challange.dto.DeveloperDTO;
import challange.mapper.DeveloperMapper;
import challange.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class DeveloperController {
    @Autowired
    DeveloperService developerService;
    @Autowired
    DeveloperMapper mapper;


    @GetMapping("/developers")
    public List<DeveloperDTO> getAllDevelopers() {
        return developerService.findAll().stream().map(mapper::convertToDTO).collect(Collectors.toList());
    }

    @RequestMapping(value = "/developers/{id}", method = RequestMethod.GET)
    public DeveloperDTO getDeveloper(@PathVariable("id") String id) {
        return developerService.findById(id).map(mapper::convertToDTO).orElseThrow(ResourceNotFoundException::new);
    }

    @DeleteMapping("/developers/{id}")
    public void deleteDeveloper(@PathVariable("id") String id) {
        developerService.deleteById(id);
    }

    @PostMapping("/developers")
    DeveloperDTO newDeveloper(@RequestBody DeveloperDTO developerDTO) {
        return mapper.convertToDTO(developerService.save(mapper.convertToEntity(developerDTO)));
    }

    @RequestMapping(value = "/developers", method = RequestMethod.PUT)
    public DeveloperDTO updateDeveloper(@RequestBody DeveloperDTO developerDTO) {
        return mapper.convertToDTO(developerService.save(mapper.convertToEntity(developerDTO)));
    }
}

