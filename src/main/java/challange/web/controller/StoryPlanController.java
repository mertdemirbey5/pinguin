package challange.web.controller;

import challange.dto.DeveloperStoryPlan;
import challange.service.TaskPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StoryPlanController {

    @Autowired
    private TaskPlanService taskPlanService;

    @GetMapping("/storyplan")
    public List<DeveloperStoryPlan> getStoryPlan() {
        return taskPlanService.getPlans();
    }
}
