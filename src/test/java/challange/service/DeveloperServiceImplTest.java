package challange.service;

import challange.entity.Developer;
import challange.repository.DeveloperRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class DeveloperServiceImplTest {

    public static final String DEVELOPER_ALEX_ID = "41828a52-44b9-44f5-8bbd-7665c2d7fe52";

    @TestConfiguration
    static class DeveloperServiceImplTestContextConfiguration {
        @Bean
        public DeveloperService developerService() {
            return new DeveloperServiceImpl();
        }
    }


    @Autowired
    private DeveloperService developerService;

    @MockBean
    private DeveloperRepository developerRepository;


    @Before
    public void setUp() {
        Developer alex = new Developer(null, "alex");
        alex.setId(DEVELOPER_ALEX_ID);

        Mockito.when(developerRepository.findById(alex.getId()))
                .thenReturn(Optional.of(alex));

        Developer john = new Developer(null, "john");
        Mockito.when(developerRepository.findAll())
                .thenReturn(Arrays.asList(alex, john));
    }

    @Test
    public void shouldFindAllReturnAllStories() {
        List<Developer> developers = developerService.findAll();
        assertEquals(2, developers.size());
    }

    @Test
    public void shouldFindByIdReturnDeveloper() {
        Optional<Developer> developer = developerService.findById("41828a52-44b9-44f5-8bbd-7665c2d7fe52");
        assertEquals(developer.get().getName(), "alex");
    }

    @Test
    public void deleteDeveloperShouldWork() {
        developerService.deleteById(DEVELOPER_ALEX_ID);
        Mockito.verify(developerRepository).deleteById(DEVELOPER_ALEX_ID);
    }

    @Test
    public void saveBugShouldWork() {
        Developer newDeveloper = new Developer(null, "newbei");
        developerService.save(newDeveloper);
        Mockito.verify(developerRepository).save(newDeveloper);
    }
}
