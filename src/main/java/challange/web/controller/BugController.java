package challange.web.controller;

import challange.dto.BugDTO;
import challange.entity.Bug;
import challange.mapper.BugMapper;
import challange.service.BugService;
import challange.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class BugController {

    @Autowired
    private BugService bugService;

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private BugMapper mapper;

    @GetMapping("/bugs")
    public List<BugDTO> getAllBugs() {
        return bugService.findAll().stream().map(mapper::convertToDTO).collect(Collectors.toList());
    }

    @RequestMapping(value = "/bugs/{id}", method = RequestMethod.GET)
    public BugDTO getBug(@PathVariable("id") String id) {
        return bugService.findById(id).map(mapper::convertToDTO).orElseThrow(ResourceNotFoundException::new);
    }

    @DeleteMapping("/bugs/{id}")
    public void deleteBug(@PathVariable("id") String id) {
        bugService.deleteById(id);
    }

    @PostMapping("/bugs")
    BugDTO newBug(@RequestBody BugDTO bugDTO) {
        return mapper.convertToDTO(bugService.save(mapper.convertToEntity(bugDTO)));
    }

    @RequestMapping(value = "/bugs", method = RequestMethod.PUT)
    public BugDTO updateBugr(@RequestBody BugDTO bugDTO) {
        return mapper.convertToDTO(bugService.save(mapper.convertToEntity(bugDTO)));
    }

    @PatchMapping("/bugs/{id}/developers/{developerId}")
    public Bug partialUpdateName(
            @PathVariable("id") String id, @PathVariable("developerId") String developerId) {
        return bugService.updateBugOwner(id, developerId).orElseThrow(ResourceNotFoundException::new);
    }
}
