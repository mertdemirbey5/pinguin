package challange.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IssueInfo {
    private String issueId; //consist with string + id
    private String title;
    private String description;
    private LocalDateTime creationDate;
    @OneToOne
    private Developer assignedTo;
}
