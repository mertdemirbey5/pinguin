package challange.service;

import challange.entity.Developer;

import java.util.List;
import java.util.Optional;

public interface DeveloperService {
    List<Developer> findAll();

    Optional<Developer> findById(String id);

    void deleteById(String id);

    Developer save(Developer developer);
}
