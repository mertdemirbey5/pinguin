package challange.service;

import challange.entity.Story;

import java.util.List;
import java.util.Optional;

public interface StoryService {
    List<Story> findAll();

    Optional<Story> findById(String id);

    void deleteById(String id);

    Story save(Story story);

    List<Story> findEstimatedStories();
}
