package challange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

//    @Autowired
//    private DeveloperService developerService;
//
//    @Autowired
//    private BugService bugService;

//    @Autowired
//    private InsertData insertData;

    public Application() {
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

//    @Bean
//    public CommandLineRunner demo() {
//        return (args) -> {
//            insertData.insert();
//        };
//    }


//   @Bean
//    public CommandLineRunner demo(DeveloperRepository developerService, BugRepository bugService) {
//        return (args) -> {
//
//            developerService.save(Developer.builder().name(("Jack")).build());
//            developerService.save(Developer.builder().name(("Chloe")).build());
//            developerService.save(Developer.builder().name(("Kim")).build());
//            developerService.save(Developer.builder().name(("David")).build());
//            developerService.save(Developer.builder().name(("Michelle")).build());
//
//            Bug bug = new Bug();
//            bug.setPriority(BugPriority.CRITICAL);
//            bug.setStatus(BugStatus.NEW);
//            IssueInfo issueInfo = new IssueInfo();
//            issueInfo.setCreationDate(LocalDateTime.now());
//            issueInfo.setDescription("issue description");
//            issueInfo.setIssueId("REAM-289");
//            issueInfo.setTitle("issute title 1");
//            bug.setIssueInfo(issueInfo);
//            bugService.save(bug);
//        };
//
//    }
}
