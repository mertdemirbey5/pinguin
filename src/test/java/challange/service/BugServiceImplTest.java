package challange.service;

import challange.entity.Bug;
import challange.repository.BugRepository;
import challange.repository.DeveloperRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class BugServiceImplTest {

    public static final String BUG_ID = "41828a52-44b9-44f5-8bbd-7665c2d7fe52";

    @TestConfiguration
    static class BugerviceImplTestContextConfiguration {
        @Bean
        public BugService bugService() {
            return new BugServiceImpl();
        }
    }

    @Autowired
    private BugService bugService;

    @MockBean
    private BugRepository bugRepository;

    @MockBean
    private DeveloperRepository developerRepository;

    @Before
    public void setUp() {
        Bug bug = Bug.builder().id(BUG_ID).build();
        Mockito.when(bugRepository.findById(bug.getId()))
                .thenReturn(Optional.of(bug));

        Mockito.when(bugRepository.findAll())
                .thenReturn(Arrays.asList(bug));
    }

    @Test
    public void shouldFindAllReturnAllBugs() {
        List<Bug> bugs = bugService.findAll();
        assertEquals(1, bugs.size());
    }

    @Test
    public void shouldFindByIdReturnBug() {
        assertTrue(bugService.findById(BUG_ID).isPresent());
    }

    @Test
    public void deleteBugShouldWork() {
        bugService.deleteById(BUG_ID);
        Mockito.verify(bugRepository).deleteById(BUG_ID);
    }

    @Test
    public void saveBugShouldWork() {
        Bug bug = Bug.builder().id("97f40e95-880e-4cf8-ab79-cf3bb1dfe168").build();
        bugService.save(bug);
        Mockito.verify(bugRepository).save(bug);
    }
}
