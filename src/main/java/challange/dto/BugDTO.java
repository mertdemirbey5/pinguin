package challange.dto;

import challange.entity.BugPriority;
import challange.entity.BugStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BugDTO {
    private String id;
    private IssueInfoDTO issueInfoDTO;
    private BugStatus status;
    private BugPriority priority;
}
