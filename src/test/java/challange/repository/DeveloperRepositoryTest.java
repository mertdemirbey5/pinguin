package challange.repository;

import challange.entity.Developer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeveloperRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeveloperRepository developerRepository;

    @Test
    public void whenFindByName_thenReturnDeveloper() {
        // given
        Developer john = Developer.builder().name("John").build();

        entityManager.persist(john);
        entityManager.flush();

        // when
        List<Developer> developers = developerRepository.findByName(john.getName());
        assertEquals(1, developers.size());


        assertEquals(developers.get(0).getName(),
                john.getName());
    }
}
