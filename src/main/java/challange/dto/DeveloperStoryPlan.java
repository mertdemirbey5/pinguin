package challange.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DeveloperStoryPlan {
    DeveloperDTO developerDTO;
    List<List<WeekPlanItem>> plans;
}
