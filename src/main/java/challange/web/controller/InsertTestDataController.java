package challange.web.controller;

import challange.entity.*;
import challange.repository.BugRepository;
import challange.repository.DeveloperRepository;
import challange.service.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api")
public class InsertTestDataController {

    @Autowired
    DeveloperRepository developerService;

    @Autowired
    private BugRepository bugRepository;

    @Autowired
    private StoryService storyService;

    @GetMapping("/insertdata")
    public void getAllDevelopers() {
        developerService.save(Developer.builder().name(("Jack")).build());
        developerService.save(Developer.builder().name(("Chloe")).build());
        developerService.save(Developer.builder().name(("Kim")).build());
        developerService.save(Developer.builder().name(("David")).build());
        developerService.save(Developer.builder().name(("Michelle")).build());

        Bug bug = new Bug();
        bug.setPriority(BugPriority.CRITICAL);
        bug.setStatus(BugStatus.NEW);
        IssueInfo issueInfo = new IssueInfo();
        issueInfo.setCreationDate(LocalDateTime.now());
        issueInfo.setDescription("issue description");
        issueInfo.setIssueId("REAM-289");
        issueInfo.setTitle("issute title 1");
        bug.setIssueInfo(issueInfo);
        bugRepository.save(bug);

        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(60).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(60).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(50).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(30).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(15).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(10).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(10).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(10).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(9).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(100).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(70).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(9).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(9).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(8).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(7).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(7).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(6).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(30).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(30).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(3).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(2).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(25).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(25).status(StoryStatus.ESTIMATED).build());
        storyService.save(Story.builder().estimatedPoint(20).status(StoryStatus.ESTIMATED).build());
    }
}
