package challange.service;

import challange.entity.Story;
import challange.entity.StoryStatus;
import challange.repository.StoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class StoryServiceImplTest {

    public static final String STORY_ID = "41828a52-44b9-44f5-8bbd-7665c2d7fe52";

    @TestConfiguration
    static class StoryerviceImplTestContextConfiguration {
        @Bean
        public StoryService storyService() {
            return new StoryServiceImpl();
        }
    }

    @Autowired
    private StoryService storyService;

    @MockBean
    private StoryRepository storyRepository;

    @Before
    public void setUp() {
        Story story = Story.builder().id(STORY_ID).build();
        Mockito.when(storyRepository.findById(story.getId()))
                .thenReturn(Optional.of(story));

        Mockito.when(storyRepository.findAll())
                .thenReturn(Arrays.asList(story));
    }

    @Test
    public void shouldFindAllReturnAllStories() {
        List<Story> stories = storyService.findAll();
        assertEquals(1, stories.size());
    }

    @Test
    public void shouldFindByIdReturnAllStories() {
        assertTrue(storyService.findById(STORY_ID).isPresent());
    }

    @Test
    public void deleteStoryShouldWork() {
        storyService.deleteById(STORY_ID);
        Mockito.verify(storyRepository).deleteById(STORY_ID);
    }

    @Test
    public void saveStoryShouldWork() {
        Story story = Story.builder().id("97f40e95-880e-4cf8-ab79-cf3bb1dfe168").build();
        storyService.save(story);
        Mockito.verify(storyRepository).save(story);
    }

    @Test
    public void findEstimatedStoriesShouldWork() {
        storyService.findEstimatedStories();
        Mockito.verify(storyRepository).findByStatus(StoryStatus.ESTIMATED);
    }
}
