package challange.service;

import challange.dto.DeveloperStoryPlan;

import java.util.List;

public interface TaskPlanService {
    List<DeveloperStoryPlan> getPlans();
}
