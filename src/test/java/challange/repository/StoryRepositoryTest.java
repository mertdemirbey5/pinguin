package challange.repository;

import challange.entity.Story;
import challange.entity.StoryStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StoryRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private StoryRepository storyRepository;

    @Test
    public void when() {
        // given
        Story storyNew = Story.builder().status(StoryStatus.NEW).build();
        Story storyCompleted = Story.builder().status(StoryStatus.COMPLETED).build();
        Story storyEstimated = Story.builder().status(StoryStatus.ESTIMATED).build();

        entityManager.persist(storyNew);
        entityManager.persist(storyCompleted);
        entityManager.persist(storyEstimated);
        entityManager.flush();

        //when
        List<Story> storyList = storyRepository.findByStatus(StoryStatus.ESTIMATED);

        //then
        assertEquals(1, storyList.size());
        assertEquals(StoryStatus.ESTIMATED, storyList.get(0).getStatus());
    }
}
