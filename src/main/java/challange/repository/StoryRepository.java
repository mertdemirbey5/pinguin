package challange.repository;

import challange.entity.Story;
import challange.entity.StoryStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoryRepository extends JpaRepository<Story, String> {
    List<Story> findByStatus(StoryStatus storyStatus);
}
