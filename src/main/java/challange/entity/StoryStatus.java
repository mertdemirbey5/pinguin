package challange.entity;

public enum StoryStatus {
    NEW, ESTIMATED, COMPLETED;
}
