package challange.mapper;

import challange.dto.DeveloperDTO;
import challange.entity.Developer;
import org.springframework.stereotype.Component;

@Component
public class DeveloperMapper implements Mapper<DeveloperDTO, Developer> {

    public DeveloperDTO convertToDTO(Developer developer) {
        return DeveloperDTO.builder().id(developer.getId())
                .name(developer.getName()).build();
    }

    public Developer convertToEntity(DeveloperDTO developerDTO) {
        return Developer.builder().id(developerDTO.getId())
                .name(developerDTO.getName()).build();
    }
}
