package challange.service;

import challange.dto.DeveloperStoryPlan;
import challange.dto.WeekPlanItem;
import challange.entity.Developer;
import challange.entity.Story;
import challange.mapper.DeveloperMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskPlanServiceImpl implements TaskPlanService {

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private StoryService storyService;

    @Autowired
    private DeveloperMapper mapper;


    private static final int WEEK_DEVELOPER_POINT = 10;

    @Override
    public List<DeveloperStoryPlan> getPlans() {
        List<DeveloperStoryPlan> plans = new ArrayList<>();
        List<Story> stories = storyService.findEstimatedStories();
        List<Developer> developers = developerService.findAll();
        List<DeveloperTask> distributedStories = distributeStoriesToDevelopers(stories, developers);
        distributedStories.forEach(d -> plans.add(new DeveloperStoryPlan(mapper.convertToDTO(d.developer), distributeWorkToWeeks(d.stories))));
        return plans;
    }

    protected List<DeveloperTask> distributeStoriesToDevelopers(List<Story> stories, List<Developer> developers) {
        List<DeveloperTask> developerTasks = developers.stream().map(DeveloperTask::new).collect(Collectors.toList());
        List<Story> sortedStory = stories.stream().sorted((s1, s2) -> s2.getEstimatedPoint().compareTo(s1.getEstimatedPoint())).collect(Collectors.toList());
        sortedStory.stream().forEach(s -> getMinPointDeveloper(developerTasks).addStory(s));
        return developerTasks;
    }

    private DeveloperTask getMinPointDeveloper(List<DeveloperTask> developerTasks) {
        return developerTasks.stream().min(Comparator.comparing(d -> d.currentLoad)).get();
    }

    protected List<List<WeekPlanItem>> distributeWorkToWeeks(List<Story> stories) {
        List<List<WeekPlanItem>> plans = new ArrayList<>();
        List<WeekPlanItem> storyWork = createNewWeek(plans);
        int weekWork = WEEK_DEVELOPER_POINT;
        for (int i = 0; i < stories.size(); ) {
            Story story = stories.get(i);
            int work = Math.min(story.getEstimatedPoint(), weekWork);
            weekWork -= work;
            storyWork.add(new WeekPlanItem(story, work));
            story.setEstimatedPoint(story.getEstimatedPoint() - work);
            if (weekWork == 0) {
                storyWork = createNewWeek(plans);
                weekWork = WEEK_DEVELOPER_POINT;
            }
            if (story.getEstimatedPoint() == 0) {
                i++;
            }
        }
        return plans;
    }

    private static List<WeekPlanItem> createNewWeek(List<List<WeekPlanItem>> plans) {
        List<WeekPlanItem> storyWork = new ArrayList<>();
        plans.add(storyWork);
        return storyWork;
    }

    static class DeveloperTask {
        private Developer developer;
        private List<Story> stories = new ArrayList<>();
        private Integer currentLoad = 0;

        public DeveloperTask(Developer developer) {
            this.developer = developer;
        }

        void addStory(Story story) {
            stories.add(story);
            currentLoad += story.getEstimatedPoint();
        }

    }
}


