package challange.service;

import challange.entity.Bug;
import challange.entity.Developer;
import challange.repository.BugRepository;
import challange.repository.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BugServiceImpl implements BugService {
    @Autowired
    private BugRepository bugRepository;
    @Autowired
    private DeveloperRepository developerRepository;

    @Override
    public List<Bug> findAll() {
        return bugRepository.findAll();
    }

    @Override
    public Optional<Bug> findById(String id) {
        return bugRepository.findById(id);
    }

    @Override
    public void deleteById(String id) {
        bugRepository.deleteById(id);
    }

    @Override
    public Bug save(Bug bug) {
        return bugRepository.save(bug);
    }

    @Override
    public Optional<Bug> updateBugOwner(String bugId, String developerId) {
        Optional<Bug> optionalBug = bugRepository.findById(bugId);
        Optional<Developer> optionalDeveloper = developerRepository.findById(developerId);
        if (optionalDeveloper.isPresent() && optionalBug.isPresent()) {
            Bug bug = optionalBug.get();
            bug.getIssueInfo().setAssignedTo(optionalDeveloper.get());
            return Optional.of(bugRepository.save(bug));
        }
        return Optional.empty();
    }
}
