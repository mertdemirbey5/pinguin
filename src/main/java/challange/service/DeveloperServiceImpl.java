package challange.service;

import challange.entity.Developer;
import challange.repository.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    DeveloperRepository developerRepository;

    @Override
    public List<Developer> findAll() {
        return developerRepository.findAll();
    }

    @Override
    public Optional<Developer> findById(String id) {
        return developerRepository.findById(id);
    }

    @Override
    public void deleteById(String id) {
        developerRepository.deleteById(id);
    }

    @Override
    public Developer save(Developer developer) {
        return developerRepository.save(developer);
    }
}
